# Package

version       = "0.1.0"
author        = "Anubhav (eagledot)"
description   = "webrtcvad (google's opensourced voice activity detection engine) bindings for Nim"
license       = "MIT"
srcDir        = "src"


# Dependencies
requires "nim >= 1.0.0"


skipDirs = @["examples"]