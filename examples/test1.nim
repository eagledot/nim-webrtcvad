#HOW TO RUN :   nim c -f -d:release -r ./test1.nim   [-f is optional for forced recompilation]

import webrtcvad

#NOTE: AudioData passed to the isSpeech function must be either 10 ms ,20 ms or 30 ms duration
#SampleRate of audioData must be  in [8000|16000|32000|48000]
#Must be  int16  PCM data..SINGLE CHANNEL.


#Declare/create a vad instance/object
#Initialize the vad object.
#setMode  --allowed values  [0|1|2|3] 3 most robust to noise (Recommended)
#check if voiceActivity  using isSpeech 

var vad: vadObj

var code = initVad(vad)
echo("init_flag value: ",vad.init_flag)  #Must be 42 after successful Initialization
#set mode
code = setMode(vad,3'i32)
assert code == 0
#check process function ...try it with fake Data.
let sampleRate = 16000*2
let frameDuration = 30 #in ms

#for this fakeData should return 0 
let fakeData = newSeq[int16](int((frameDuration*sampleRate)/1000))

#returns 1 if voice activity detected else 0 but returns -1 when some error occured.
code = isSpeech(vad,fakeData,sampleRate)
echo(code)
echo("Done")
