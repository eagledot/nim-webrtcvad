
#nim binding for webrtcvad
#No function for freeing VAD instance ..as taken Care by the NIM GC.

{.compile: "src_c/simple.c".}


const kNumChannels = 6'i32   # Number of frequency bands (named channels).
const  kNumGaussians = 2'i32 # Number of Gaussians per channel in the GMM.
const kTableSize =  kNumChannels * kNumGaussians 
const kMinEnergy = 10'i32    #Minimum energy required to trigger audio signal.
type
    WebRtcSpl_State48khzTo8khz_nim = object
        S_48_24: array[8,cint]
        S_24_24: array[16,cint]
        S_24_16: array[8,cint]
        S_16_8 : array[8,cint]
type 
    vadObj*   = object
    #has various fields.
        vad: cint
        downsampling_filter_states: array[4,int32]
        state_48_to_8: WebRtcSpl_State48khzTo8khz_nim
        noise_means : array[kTableSize,int16]
        speech_means: array[kTableSize,int16]
        noise_stds: array[kTableSize,int16]
        speech_stds: array[kTableSize,int16]
        frame_counter: int32
        over_hang: int16
        num_of_speech: int16
        index_vector: array[16*kNumChannels,int16]
        low_value_vector: array[16*kNumChannels,int16]
        mean_value: array[kNumChannels,int16]
        upper_state: array[5,int16]
        lower_state: array[5,int16]
        hp_filter_state: array[4,int16]
        over_hang_max_1: array[3,int16]
        over_hang_max_2: array[3,int16]
        individual: array[3,int16]
        total: array[3,int16]

        init_flag*: cint


proc initVad(handle: ptr vadObj): cint {.importc: "WebRtcVad_InitCore",cdecl.}
proc initVad*(obj: vadObj): cint =
    return initVad(unsafeAddr(obj))
#set aggressiveness degree
proc setMode(handle: ptr vadObj,mode: cint): cint {.importc: "WebRtcVad_set_mode_core",cdecl.}
proc setMode*(obj: vadObj,mode: cint):cint =
    return setMode(unsafeAddr(obj),mode)
proc CalcVad48khz(handle: ptr vadObj,audioData: ptr int16,nSamples: culonglong): cint {.importc: "WebRtcVad_CalcVad48khz",cdecl.}
proc CalcVad32khz(handle: ptr vadObj,audioData: ptr int16,nSamples: culonglong): cint {.importc: "WebRtcVad_CalcVad32khz",cdecl.}
proc CalcVad16khz(handle: ptr vadObj,audioData: ptr int16,nSamples: culonglong): cint {.importc: "WebRtcVad_CalcVad16khz",cdecl.}
proc CalcVad8khz(handle: ptr vadObj,audioData: ptr int16,nSamples: culonglong): cint {.importc: "WebRtcVad_CalcVad8khz",cdecl.}


proc checkSamplerate_len(sampleRate: uint,nSamples: culonglong): bool =
    if sampleRate == 48000:
        return nSamples in [480'u64,960,1440]
    elif sampleRate == 32000:
        return nSamples in [320'u64,640,960]
    elif sampleRate == 16000:
        return nSamples in [160'u64,320,480]
    elif sampleRate == 8000:
        return nSamples in [80'u64,160,320]
    else:
        return false


#REMEMBER :  10 ms,20ms,30 ms amount of time is allowed with 8khz,16khz,32khz,48khz sample Rate...16bit PCM data.
proc vadProcess(handle: ptr vadObj,sampleRate: uint,audioData: ptr int16,nSamples: culonglong):cint =
    #nSamples represents the number of samples..where each sample is a 16 bit integer. 
    #based on the sampleRate call the appropriate function.
    #like if sampleRate is 16khz allowed nSamples = ((10|20|30) / 1000)* 16000 i.e 160,320,480

    if checkSamplerate_len(sampleRate,nSamples) == false:
        echo("SampleRate or frameLen mismatch")
        return -1

    if (sampleRate == 48000):
        result = CalcVad48khz(handle,audioData,nSamples)
    elif (sampleRate == 32000):
        result = CalcVad32khz(handle,audioData,nSamples)
    elif (sampleRate == 16000):
        result = CalcVad16khz(handle,audioData,nSamples)
    else:
        result = CalcVad8khz(handle,audioData,nSamples)

    if result > 0'i32:
        return 1'i32
    elif result == 0'i32:
        return 0'i32
    else:
        return -1'i32

proc vadProcess(obj: vadObj,audioData: ptr int16, sampleRate: int, nSamples: int): cint =
    return vadProcess(unsafeAddr(obj),uint(sampleRate), audioData, culonglong(nSamples))

#Cleaner Api
proc isSpeech*(obj: vadObj ,audioData: seq[int16],sampleRate: int): cint = 
    return vadProcess(obj,unsafeAddr(audioData[0]),sampleRate,len(audioData))
    

    
    


when isMainModule:
    var vad: vadObj
    var code = initVad(vad)
    echo("init_flag value: ",vad.init_flag)  #Must be 42 after successful Initialization
    #set mode Only allowed values are [0,1,2,3] recommended 3
    code = setMode(vad,3'i32)
    assert code == 0
    #check process function ...try it with fake Data.
    let sampleRate = 16000*2
    let frameDuration = 30 #in ms

    let fakeData = newSeq[int16](int((frameDuration*sampleRate)/1000))
    #code = vadProcess(addr(vad),uint(sampleRate),unsafeAddr(fakeData[0]),uint(len(fakeData)))
    code = isSpeech(vad,fakeData,sampleRate)
    echo(code)
    echo("Done")




    






